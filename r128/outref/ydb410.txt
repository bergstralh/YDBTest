---------------------------------------------------------------------------
ydb410: Tests forwarding (from YDB to non-YDB base program) of all signals
        except SIGTSTP, SIGTTIO, and SIGTTOU (which cause process suspension)

** First build ydb410 executable to create a handler for a given signal and see
** if it gets forwarded or not. Note either answer (yes it was forwarded or no
** it was not) can be the correct answer depending on the setup. An 'invald
** argument' reply is also possible for the signals that the OS disallows
** setting a sigaction() handler for. Also SIGCHLD is a signal we do not allow
** to be forwarded. It is also unconditionally installed with an 'ignore' type
** handler so it's forwarding won't work.

Driving ydb410 SIGHUP (1):
ydb410: Signal 1 successfully received in our handler
**********************
Driving ydb410 SIGINT (2):
ydb410: Signal 2 successfully received in our handler
**********************
Driving ydb410 SIGQUIT (3):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 3 from process [0-9]* with userid number [0-9]*
ydb410: Signal 3 successfully received in our handler
**********************
Driving ydb410 SIGILL (4):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 4 from process [0-9]* with userid number [0-9]*
ydb410: Signal 4 successfully received in our handler
**********************
Driving ydb410 SIGTRAP (5):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 5 from process [0-9]* with userid number [0-9]*
ydb410: Signal 5 successfully received in our handler
**********************
Driving ydb410 SIGABRT (6):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 6 from process [0-9]* with userid number [0-9]*
ydb410: Signal 6 successfully received in our handler
**********************
Driving ydb410 SIGBUS (7):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 7 from process [0-9]* with userid number [0-9]*
ydb410: Signal 7 successfully received in our handler
**********************
Driving ydb410 SIGFPE (8):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 8 from process [0-9]* with userid number [0-9]*
ydb410: Signal 8 successfully received in our handler
**********************
Driving ydb410 SIGKILL (9):
ydb410: Failure to set sigaction for signal 9: Invalid argument
**********************
Driving ydb410 SIGUSR1 (10):
ydb410: Signal 10 successfully received in our handler
**********************
Driving ydb410 SIGSEGV (11):
##TEST_AWK%YDB-F-KILLBYSIGUINFO, YottaDB process [0-9]* has been killed by a signal 11 from process [0-9]* with userid number [0-9]*
ydb410: Signal 11 successfully received in our handler
**********************
Driving ydb410 SIGUSR2 (12):
ydb410: Signal 12 successfully received in our handler
**********************
Driving ydb410 SIGPIPE (13):
ydb410: Signal 13 successfully received in our handler
**********************
Driving ydb410 SIGALRM (14):
ydb410: Signal 14 successfully received in our handler
**********************
Driving ydb410 SIGTERM (15):
%YDB-F-FORCEDHALT, Image HALTed by MUPIP STOP
ydb410: Signal 15 successfully received in our handler
**********************
Driving ydb410 SIGSTKFLT (16):
ydb410: Signal 16 successfully received in our handler
**********************
Driving ydb410 SIGCHLD (17):
ydb410: Timeout - Did not get signal 17 interrupt in 3 seconds
**********************
Driving ydb410 SIGCONT (18):
ydb410: Timeout - Did not get signal 18 interrupt in 3 seconds
**********************
Driving ydb410 SIGSTOP (19):
ydb410: Failure to set sigaction for signal 19: Invalid argument
**********************
Driving ydb410 SIGURG (23):
ydb410: Signal 23 successfully received in our handler
**********************
Driving ydb410 SIGXCPU (24):
ydb410: Signal 24 successfully received in our handler
**********************
Driving ydb410 SIGXFSZ (25):
ydb410: Signal 25 successfully received in our handler
**********************
Driving ydb410 SIGVTALRM (26):
ydb410: Signal 26 successfully received in our handler
**********************
Driving ydb410 SIGPROF (27):
ydb410: Signal 27 successfully received in our handler
**********************
Driving ydb410 SIGWINCH (28):
ydb410: Signal 28 successfully received in our handler
**********************
Driving ydb410 SIGIO (29):
ydb410: Signal 29 successfully received in our handler
**********************
Driving ydb410 SIGPWR (30):
ydb410: Signal 30 successfully received in our handler
**********************
Driving ydb410 SIGSYS (31):
ydb410: Signal 31 successfully received in our handler
**********************
Driving ydb410 SIGunknown (32):
ydb410: Failure to set sigaction for signal 32: Invalid argument
**********************
Driving ydb410 SIGunknown (33):
ydb410: Failure to set sigaction for signal 33: Invalid argument
**********************
Driving ydb410 SIGRTMIN (34):
ydb410: Signal 34 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+1 (35):
ydb410: Signal 35 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+2 (36):
ydb410: Signal 36 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+3 (37):
ydb410: Signal 37 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+4 (38):
ydb410: Signal 38 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+5 (39):
ydb410: Signal 39 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+6 (40):
ydb410: Signal 40 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+7 (41):
ydb410: Signal 41 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+8 (42):
ydb410: Signal 42 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+9 (43):
ydb410: Signal 43 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+10 (44):
ydb410: Signal 44 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+11 (45):
ydb410: Signal 45 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+12 (46):
ydb410: Signal 46 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+13 (47):
ydb410: Signal 47 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+14 (48):
ydb410: Signal 48 successfully received in our handler
**********************
Driving ydb410 SIGRTMIN+15 (49):
ydb410: Signal 49 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-14 (50):
ydb410: Signal 50 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-13 (51):
ydb410: Signal 51 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-12 (52):
ydb410: Signal 52 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-11 (53):
ydb410: Signal 53 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-10 (54):
ydb410: Signal 54 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-9 (55):
ydb410: Signal 55 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-8 (56):
ydb410: Signal 56 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-7 (57):
ydb410: Signal 57 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-6 (58):
ydb410: Signal 58 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-5 (59):
ydb410: Signal 59 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-4 (60):
ydb410: Signal 60 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-3 (61):
ydb410: Signal 61 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-2 (62):
ydb410: Signal 62 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX-1 (63):
ydb410: Signal 63 successfully received in our handler
**********************
Driving ydb410 SIGRTMAX (64):
ydb410: Signal 64 successfully received in our handler
**********************

---------------------------------------------------------------------------

Test forwarding for SIGALRM and SIGUSR1 after also adding them to the NOFWD list

%YDB-E-INVSIGNM, Invalid signal number/name/abbreviation specified in $ydb_signal_fwd: notasig
%YDB-E-INVSIGNM, Invalid signal number/name/abbreviation specified in $ydb_signal_nofwd: signotis
ydb410: Timeout - Did not get signal 14 interrupt in 3 seconds
%YDB-E-INVSIGNM, Invalid signal number/name/abbreviation specified in $ydb_signal_fwd: notasig
%YDB-E-INVSIGNM, Invalid signal number/name/abbreviation specified in $ydb_signal_nofwd: signotis
ydb410: Timeout - Did not get signal 10 interrupt in 3 seconds
